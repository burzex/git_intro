package com.example.demo.controller;

import com.example.demo.dao.CourseRepository;
import com.example.demo.domain.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
@RequestMapping("/course")
public class CourseController {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    // возвращает таблицу курсов
//    @RequestMapping
//    public String courseTable(Model model) {
//        model.addAttribute("courses", courseRepository.findAll());
//        return "course_table";
//    }

    @RequestMapping
    public String courseTable(Model model, @RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        if (titlePrefix != null) {
            model.addAttribute("courses", courseRepository.findByTitleWithPrefix(titlePrefix));
        } else {
            model.addAttribute("courses", courseRepository.findAll());
        }
        return "course_table";
    }

    // возвращает форму просмотра/редактирования курса
    @RequestMapping("/{id}")
    public String courseForm(Model model, @PathVariable("id") Long id) {
        model.addAttribute("course", courseRepository.findById(id).orElseThrow(NotFoundException::new));
        return "course_form";
    }

    // валидация при сохранении курса
    @PostMapping
    public String submitCourseForm(@Valid Course course, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "course_form";
        }
        courseRepository.save(course);
        return "redirect:/course";
    }

    // форма создания курса
    @RequestMapping("/new")
    public String courseForm(Model model) {
        model.addAttribute("course", new Course());
        return "course_form";
    }

    // удаление курса
    @DeleteMapping("/{id}")
    public String deleteCourse(@PathVariable("id") Long id) {
        courseRepository.delete(id);
        return "redirect:/course";
    }

    // форма исключения при обращении к отсутствующему курсу
    @ExceptionHandler
    public ModelAndView notFoundExceptionHandler(NotFoundException ex) {
        ModelAndView modelAndView = new ModelAndView("not_found");
        modelAndView.setStatus(HttpStatus.NOT_FOUND);
        return modelAndView;
    }

}